import java.util.*;

public class Main {
    public static void main(String[] args) {
        SinglyLinkedList<Integer> digits = new SinglyLinkedList<>();

        digits.add(1);
        digits.add(5);
        System.out.println("First: " + digits.get(0));
        System.out.println("Second: " + digits.get(1));

        digits.add(8);
        System.out.println("Third: " + digits.get(2));

        System.out.println("Size: " + digits.size());

//        digits.remove(digits.size() - 1);
//        digits.remove(digits.size() / 2);
//        digits.remove(0);
        System.out.println("Size: " + digits.size());

        System.out.println("First: " + digits.get(0));
        System.out.println("Second: " + digits.get(1));


        Iterator listIterSingly = digits.iterator();

        System.out.println("ITERATOR");
        while (listIterSingly.hasNext()) {
            System.out.println(listIterSingly.next());
        }
        listIterSingly.remove();
        System.out.println("Size: " + digits.size());

        System.out.println("First: " + digits.get(0));
        System.out.println("Second: " + digits.get(1));


        System.out.println("DOUBLELINKEDLIST");
        DoubleLinkedList<Integer> numbers = new DoubleLinkedList<>();

        numbers.addFirst(1);
        numbers.addFirst(5);
        numbers.addLast(8);

        System.out.println("First: " + numbers.getFirst());
        System.out.println("Second: " + numbers.getLast());

        numbers.removeFirst();
        numbers.removeLast();
        System.out.println("First: " + numbers.getFirst());
        System.out.println("Second: " + numbers.getLast());


        ListIterator listIterDouble = numbers.iterator();
        numbers.addLast(9);
        numbers.addLast(9);
        numbers.addLast(9);

        System.out.println("ITERATOR DOUBLE");
        while (listIterDouble.hasNext()) {
            System.out.println(listIterDouble.next());
        }
        System.out.println("REMOVE");
        listIterDouble.remove();
        while (listIterDouble.hasPrevious()) {
            System.out.println(listIterDouble.previous());
        }

        System.out.println("Size: " + numbers.size());

    }
}