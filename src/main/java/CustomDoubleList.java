
public interface CustomDoubleList<E> {

    int size();

    void addFirst(E element);

    void addLast(E element);

    E getFirst();

    E getLast();

    E removeFirst();

    E removeLast();


}