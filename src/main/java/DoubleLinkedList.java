
import java.util.*;

public class DoubleLinkedList<E> implements CustomDoubleList<E> {

    private int size;
    private Node<E> first;
    private Node<E> last;

    @Override
    public int size() {
        return size;
    }

    @Override
    public void addFirst(E element) {
        linkFirst(element);
    }

    @Override
    public void addLast(E element) {
        linkLast(element);
    }

    private void linkFirst(E e) {
        final Node<E> f = first;
        final Node<E> newNode = new Node<>(null, e, f);
        first = newNode;
        if (f == null)
            last = newNode;
        else
            f.prev = newNode;
        size++;
    }

    void linkLast(E e) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, e, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
    }

    @Override
    public E getFirst() {
        return this.first.value;
    }

    @Override
    public E getLast() {
        return this.last.value;
    }

    @Override
    public E removeFirst() {
        final Node<E> f = first;
        return unlinkFirst(f);
    }

    private E unlinkFirst(Node<E> f) {
        final E element = f.value;
        final Node<E> next = f.next;
        f.value = null;
        f.next = null;
        first = next;
        if (next == null)
            last = null;
        else
            next.prev = null;
        size--;
        return element;
    }

    @Override
    public E removeLast() {
        final Node<E> l = last;
        if (l == null)
            throw new NoSuchElementException();
        return unlinkLast(l);
    }

    private E unlinkLast(Node<E> l) {
        final E element = l.value;
        final Node<E> prev = l.prev;
        l.value = null;
        l.prev = null;
        last = prev;
        if (prev == null)
            first = null;
        else
            prev.next = null;
        size--;
        return element;
    }

    public ListIterator<E> iterator() {
        return new DoubleListIterator();
    }


    private static class Node<E> {
        E value;
        Node<E> next;
        Node<E> prev;

        public Node(Node<E> prev, E value, Node<E> next) {
            this.value = value;
            this.next = next;
            this.prev = prev;
        }


    }

    private class DoubleListIterator implements ListIterator<E> {
        private Node<E> lastReturned;
        int cursor;

        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        @Override
        public E next() {
            Node<E> current = DoubleLinkedList.this.first;
            for (int i = 1; i <= cursor; i++) {
                current = current.next;
            }
            this.cursor++;
            this.lastReturned = current;
            return current.value;
        }

        @Override
        public boolean hasPrevious() {
            return cursor != 0;
        }

        @Override
        public E previous() {
            E previous = lastReturned.prev.value;
            this.lastReturned = lastReturned.prev;
            cursor--;
            return previous;
        }

        @Override
        public int nextIndex() {
            return cursor++;
        }

        @Override
        public int previousIndex() {
            return cursor--;
        }

        @Override
        public void remove() {
            DoubleLinkedList.this.unlinkFirst(this.lastReturned);
            this.cursor--;
        }

        @Override
        public void set(E e) {
            this.lastReturned.value = e;

        }

        @Override
        public void add(E e) {
            lastReturned = null;
            if (DoubleLinkedList.this.first.next.value == null)
                linkLast(e);
            else
                linkFirst(e);
        }
    }
}